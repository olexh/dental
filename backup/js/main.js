const apiUrl = "https://dentaljar.com/"
var accessToken = window.localStorage.getItem("access_token");
var user = {};
var app;
const methods = {
	getParams: (async) => createRequest("api/params/get", "POST", null, async),
	addCategory: (params, async = true) => createRequest("api/categories/add", "POST", params, async),
	getCategory: (params, async = true) => createRequest("api/categories/get", "POST", params, async),
	removeCategory: (params, async = true) => createRequest("api/categories/remove", "POST", params, async),
	editCategory: (params, async = true) => createRequest("api/categories/edit", "POST", params, async),
	getSubcategories: (params, async = true) => createRequest("api/categories/get/subcategories", "POST", params, async),
	getCategories: (async = true) => createRequest("api/categories/getall", "POST", null, async),
	getItems: (async = true) => createRequest("api/items/getall", "POST", null, async),
	getExpiryItems: (async = true) => createRequest("api/items/getexpiry", "POST", null, async),
	getLowItems: (async = true) => createRequest("api/items/getlow", "POST", null, async),
	getAlmostLowItems: (async = true) => createRequest("api/items/getalmostlow", "POST", null, async),
	getItemsByCategory: (params, async = true) => createRequest("api/items/getbycategory", "POST", params, async),
	getItemsInCategory: (params, async = true) => createRequest("api/items/getincategory", "POST", params, async),
	getItem: (params, async = true) => createRequest("api/items/get", "POST", params, async),
	addItem: (params, async = true) => createRequest("api/items/add", "POST", params, async),
	removeItem: (params, async = true) => createRequest("api/items/remove", "POST", params, async),
	editItem: (params, async = true) => createRequest("api/items/edit", "POST", params, async),
	editItemQuantity: (params, async = true) => createRequest("api/items/edit/quantity", "POST", params, async),
	login: (params, async = true) => createRequest("api/login", "POST", params, async),
	logout: (params, async = true) => createRequest("api/logout", "GET", params, async),
	signup: (params, async = true) => createRequest("api/signup", "POST", params, async),
	user: (async = true) => createRequest("api/user", "GET", null, async),
	contactUs: (params, async = true) => createRequest("api/contactus", "POST", params, async),
}

function createRequest(url, type = 'POST', data = null, async = true) {
	return $.ajax({
		headers: {
			'X-Requested-With': 'XMLHttpRequest',
	        'Authorization': `Bearer ${accessToken}`,
	    },
	    url: apiUrl + url,
		async,
        type,
        data
    })
}

function mountApp() {
	app = riot.mount('app')
	route.stop()
	route.base('/')
	route.start(true)
}

methods.user().done((data) => {
	user = data;		
}).fail((error) => {
	user = null;
}).always((data) => {
	mountApp();
	$('#overlay').fadeOut();
})
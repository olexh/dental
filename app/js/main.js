const apiUrl = "https://dentaljar.com/"
var accessToken = window.localStorage.getItem("access_token");
var types = [{id: 1, name: "Box"}, {id: 2, name: "Pack"}, {id: 3, name: "Tube"}, {id: 4, name: "Bottle"}, {id: 5, name: "Singular"}];
var user = {};
var items = [], categories = [];
var app;

const methods = {
	getParams: (async) => createRequest("api/params/get", "POST", null, async),
	addCategory: (params, async = true) => createRequest("api/categories/add", "POST", params, async),
	getCategory: (params, async = true) => createRequest("api/categories/get", "POST", params, async),
	removeCategory: (params, async = true) => createRequest("api/categories/remove", "POST", params, async),
	editCategory: (params, async = true) => createRequest("api/categories/edit", "POST", params, async),
	getSubcategories: (params, async = true) => createRequest("api/categories/get/subcategories", "POST", params, async),
	getCategories: (async = true) => createRequest("api/categories/getall", "POST", null, async),
	getItems: (async = true) => createRequest("api/items/getall", "POST", null, async),
	getExpiryItems: (async = true) => createRequest("api/items/getexpiry", "POST", null, async),
	getLowItems: (async = true) => createRequest("api/items/getlow", "POST", null, async),
	getAlmostLowItems: (async = true) => createRequest("api/items/getalmostlow", "POST", null, async),
	getItemsByCategory: (params, async = true) => createRequest("api/items/getbycategory", "POST", params, async),
	getItemsInCategory: (params, async = true) => createRequest("api/items/getincategory", "POST", params, async),
	getItem: (params, async = true) => createRequest("api/items/get", "POST", params, async),
	addItem: (params, async = true) => createRequest("api/items/add", "POST", params, async),
	removeItem: (params, async = true) => createRequest("api/items/remove", "POST", params, async),
	editItem: (params, async = true) => createRequest("api/items/edit", "POST", params, async),
	editItemQuantity: (params, async = true) => createRequest("api/items/edit/quantity", "POST", params, async),
	login: (params, async = true) => createRequest("api/login", "POST", params, async),
	logout: (params, async = true) => createRequest("api/logout", "GET", params, async),
	signup: (params, async = true) => createRequest("api/signup", "POST", params, async),
	user: (async = true) => createRequest("api/user", "GET", null, async),
	contactUs: (params, async = true) => createRequest("api/contactus", "POST", params, async),
	getClinics: (async = true) => createRequest("api/clinics/getall"),
	addClinic: (params, async = true) => createRequest("api/clinics/add", "POST", params, async),
	removeClinic: (params, async = true) => createRequest("api/clinics/remove", "POST", params, async)
}

function createRequest(url, type = 'POST', data = null, async = true) {
	return $.ajax({
		headers: {
			'X-Requested-With': 'XMLHttpRequest',
	        'Authorization': `Bearer ${accessToken}`,
	    },
	    url: apiUrl + url,
		async,
        type,
        data
    })
}

function rebuildCategories(data) {
	for (var i = 0; i < data.length; i++) {
		data[i].category = data[i].category.name;
	}
	
	return data;
}

function getSubcategories(params) {
	let data = categories.filter((category) => {
		return category.parent_category_id == params.id;
	})

	if (data) {
		return data;
	}

	return [];
}

function getCategory(params) {
	let data = categories.find((category) => {
		return category.id == params.id;
	});

	if (data) {
		return data;
	}

	return {};
}

function getItems(params) {
	let data = items.filter((item) => {
		return item.category_id == params.category_id;
	})

	if (data) {
		return data;
	}

	return [];
}

function getLowStockItems() {
	let data = items.filter((item) => {
		return parseInt(item.quantity) <= parseInt(item.quantity_trigger);
	})

	if (data) {
		return data;
	}

	return [];
}


function mountApp() {
	app = riot.mount('app');
	route.stop();
	route.base('/');
	route.start(true);
}

methods.user().done((data) => {
	user = data;
	methods.getItems().then((data) => {
		items = rebuildCategories(data);
	}).then(() => {
		methods.getCategories().then((data) => {
			categories = data;
			mountApp();

			$('#overlay').fadeOut();
		});
	});
}).fail((error) => {
	user = null;
	mountApp();
	$('#overlay').fadeOut();
})
<app>
	<app-nav if={user != null}></app-nav>
	<router if={user != null}>
		<route path="/stock"><app-stock></app-stock></route> <!-- CURRENT STOCK -->
		<route path="/stock/*"><app-stock></app-stock></route> <!-- CURRENT STOCK -->
		<route path="/manage/item/*"><manage-item></manage-item></route> <!-- MANAGE ITEMS -->
		<route path="/manage/item"><manage-item></manage-item></route> <!-- MANAGE ITEMS -->
		<route path="/manage/category"><manage-category></manage-category></route> <!-- MANAGE CATEGORIES -->
		<route path="/manage/category/*"><manage-category></manage-category></route> <!-- MANAGE CATEGORIES -->
		<route path="/add/item"><add-item></add-item></route> <!-- ADD ITEM -->
		<route path="/add/category"><add-category></add-category></route> <!-- ADD CATEGORY -->
		<route path="/summary"><summary></summary></route> <!-- SUMMARY -->
		<route path="/contact"><contact-us></contact-us></route> <!-- CONTACT US -->
		<route path="/manage/clinic" if={user.role == 2}><manage-clinic></manage-clinic></route> <!-- MANAGE CLINIC -->
		<route path="/..">{route('/stock')}</route> <!-- 404 -->
	</router>
	<router if={user == null}>
		<route path="/login"><login></login></route> <!-- LOGIN -->
		<route path="/register"><register></register></route> <!-- REGISTER -->
		<route path="/..">{route('/login')}</route> <!-- 404 -->
	</router>
	<script>
	</script>
</app>
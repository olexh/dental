<app-nav class="navbar navbar-expand-md">
	<div class="container">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navItems" aria-controls="navItems" aria-expanded="false" aria-label="Toggle navigation">
			<span class="entypo list collapse-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navItems">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="/stock">Home</a>
				</li>
				 <li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="manageDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Manage
					</a>
					<div class="dropdown-menu" aria-labelledby="manageDropdown">
						<a class="dropdown-item" href="/manage/item">Manage item</a>
						<a class="dropdown-item" href="/manage/category">Manage category</a>
						<a class="dropdown-item" href="/add/item">Add item</a>
						<a class="dropdown-item" href="/add/category">Add category</a>
					</div>
				</li>
				<li class="nav-item dropdown" if={user.role == 2}>
					<a class="nav-link dropdown-toggle" href="#" id="adminDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Admin
					</a>
					<div class="dropdown-menu" aria-labelledby="adminDropdown">
						<a class="dropdown-item" href="/manage/clinic">Manage dental clinics</a>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/summary">Summary</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/contact">Feedback</a>
				</li>
			</ul>
			<div class="mr-md-3">
				<input list="items" class='flexdatalist' id="search-input" placeholder="Search..." >
			</div>
			<div class="mt-2 mt-md-0">
				<div class="row align-items-center justify-content-center">
					<div class="col-auto">
						<p class="m-0 font-weight-bold">{user.name}</p>
						<p class="m-0">{user.clinic.name}</p>
					</div>
					<div class="col">
						<button class="btn light only-icon" onclick={logout}>
							<i class="fas fa-sign-out-alt"></i>
						</button>
					</div>
				</div>
				
			</div>
		</div>

		
	</div>
	<script>
		$( document ).ready(function() {

		    $('.flexdatalist').flexdatalist({
				data: items.concat(categories),
				minLength: 1,
				valueProperty: 'id',
				searchContain: true,
				selectionRequired: true,
				searchIn: ["name"],
				visibleProperties: ["name", "category"],
			});

			$('.flexdatalist').on('select:flexdatalist', function(event, set, options) {
				if ("parent_category_id" in set) {
					window.open("/stock/" + set.id, "_self");
				} else {
					window.open("/manage/item/" + set.id, "_self");
				}
			});
		});

		logout() {
			methods.logout();
			window.localStorage.removeItem("access_token");
			window.open("/login", "_self");
		}
	</script>
</app-nav>
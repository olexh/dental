<clinic class="col-12 col-md-3 mb-4">
	<div class="clinic">
		<div class="header">
			{opts.clinic.name}
		</div>
		<button class="btn inline only-icon w-100 px-2 mt-2" onclick={removeClinic}>
			<i class="fas fa-times"></i> Remove clinic
		</button>
	</div>

	<script>
		removeClinic() {
			methods.removeClinic({id: opts.clinic.id}).then(() => {
				window.open("/manage/clinic","_self");
			})
		}
	</script>
</clinic>


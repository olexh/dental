<item class="col-md-3 col-12 mb-4">
	<div class="item">
		<a href="/manage/item/{opts.item.id}">
			<div class="header">
				{opts.item.name}
			</div>
			<div class="tags">
				<div class="row no-gutters">
					<div class="col-auto" if={opts.item.category.name != ""}>
						<div class="tag category-tag">
							{opts.item.category}
						</div>
					</div>
					<div class="w-100" if={opts.item.category.name != ""}></div>
					<div class="col-auto" if={(parseInt(opts.item.quantity_trigger) >= parseInt(opts.item.quantity)) && !parseInt(opts.item.ordered)}>
						<div class="tag red" >
							Low stock alert
						</div>
					</div>
					<div class="col-auto" if={parseInt(opts.item.ordered)}>
						<div class="tag yellow">
							Ordered
						</div>
					</div>
					<div class="col-auto" if={opts.item.type != null && opts.item.type != 0}>
						<div class="tag">
							{typeName(opts.item.type)}
						</div>
					</div>
				</div>
			</div>
			<div class="body">
				<p class="m-0">
					<span class="font-weight-bold">{opts.item.quantity}</span> in stock
				</p>
				<p class="my-1" if={opts.item.batch_number != null}>
					Batch number <span class="font-weight-bold">{opts.item.batch_number}</span>
				</p>
				<p class="my-1 red" if={opts.expiry != null}>
					Expiry date <span class="font-weight-bold">{moment(opts.expiry).format('ll')}</span>
				</p>
				<p class="my-1">
					Last update <span class="font-weight-bold">{moment(opts.item.updated_at).format('ll')}</span>
				</p>
			</div>
		</a>
		<div class="body" if={(parseInt(opts.item.quantity_trigger) >= parseInt(opts.item.quantity)) && !parseInt(opts.item.ordered)}>
			<button class="btn inline only-icon w-100 px-2 mt-2" onclick={order}>
				<i class="fas fa-plus"></i> Order
			</button>
		</div>
		<div class="body" if={parseInt(opts.item.ordered)}>
			<button class="btn inline only-icon w-100 px-2 mt-2" onclick={cancelOrder}>
				<i class="fas fa-minus"></i> Cancel order
			</button>
		</div>
	</div>

	<script>
		order() {
			methods.editItem({...opts.item, edit_type: 4, ordered: 1}).then((data) => {
				window.open("/stock","_self");
			})
		}

		cancelOrder() {
			methods.editItem({...opts.item, edit_type: 4, ordered: 0}).then((data) => {
				window.open("/stock","_self");
			})
		}

		typeName(id) {
			return types.find(type => type.id === parseInt(opts.item.type)).name;
		}
	</script>
</item>


<contact-us>
	<div class="modal fade" id="sent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                Contact us
	            </div>
	            <div class="modal-body">
	                Mail successfully sent. We will contact with you!
	            </div>
	            <div class="modal-footer text-center">
	                <button type="button" class="btn btn-light" data-dismiss="modal" onclick={goToStock}>OK</button>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="container">
		<div class="row main-container mt-md-5 mt-3">
			<div class="col-auto">
				<h1 class="main-header">Feedback</h1>
			</div>
			<div class="w-100"></div>
			<div class="col">
				<form onsubmit={ feedback }>
					<div class="row">
						<div class="col mt-4">
							<p>Message *</p>
							<textarea ref="message" rows="3"></textarea>
						</div>
						<div class="w-100"></div>
						<div class="col mt-4 mb-4 text-center">
							<button class="btn light" type="submit">
								<span><i class="fas fa-check"></i> Submit</span>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		
	</div>
	<script>
		goToStock() {
			window.open("/stock","_self");
		}

		feedback(e) {
			e.preventDefault();
			
			$('button[type=submit], input[type=submit]').prop('disabled',true);

			if (this.refs.message.value == "") {
				$('button[type=submit], input[type=submit]').prop('disabled',false);
				return;
			}

			methods.contactUs({message: this.refs.message.value}).then((data) => {
				$("#sent").modal()
				$('button[type=submit], input[type=submit]').prop('disabled',false);
			});
		}
	</script>
</contact-us>
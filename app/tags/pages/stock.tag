<app-stock>
	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                Remove category
	            </div>
	            <div class="modal-body">
	                Are you sure?
	            </div>
	            <div class="modal-footer text-center">
	                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
	                <button type="button" class="btn btn-light" data-dismiss="modal" onclick={removeCategory}>Remove</button>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="container">
		<div class="mt-md-5 mt-3" if={expiryItems.length > 0 && !("id" in this.category)}>
			<div class="row main-container">
				<div class="col-auto">
					<a data-toggle="collapse" href="#expiryCollapse" role="button" aria-expanded="false">
					    <h1 class="main-header">Close to expiry items ({expiryItems.length}) <i class="fas fa-chevron-down"></i></h1>
					</a>
				</div>
				<div class="w-100"></div>
				<div class="col collapse" id="expiryCollapse">
					<div class="row mt-4">
						<virtual each={expiryItem in expiryItems}>
							<item item={expiryItem} expiry={expiryItem.expiry_at}></item>
						</virtual>
					</div>
				</div>
			</div>
		</div>

		<div class="mt-md-5 mt-3"  if={lowItems.length > 0 && !("id" in this.category)}>
			<div class="row main-container">
				<div class="col-auto">
					<a data-toggle="collapse" href="#lowCollapse" role="button" aria-expanded="false">
					    <h1 class="main-header">Low stock warning ({lowItems.length}) <i class="fas fa-chevron-down"></i></h1>
					</a>
				</div>
				<div class="w-100"></div>
				<div class="col collapse" id="lowCollapse">
					<div class="row mt-4">
						<virtual each={lowItem in lowItems}>
							<item item={lowItem}></item>
						</virtual>
					</div>
				</div>
			</div>
		</div>
		
		<div class="mt-md-5 mt-3" if={almostLowItems.length > 0 && !("id" in this.category)}>
			<div class="row main-container">
				<div class="col-auto">
					<a data-toggle="collapse" href="#orangeCollapse" role="button" aria-expanded="false">
					    <h1 class="main-header">Orange zone ({almostLowItems.length}) <i class="fas fa-chevron-down"></i></h1>
					</a>
				</div>
				<div class="w-100"></div>
				<div class="col collapse" id="orangeCollapse">
					<div class="row mt-4">
						<virtual each={almostLowItem in almostLowItems}>
							<item item={almostLowItem}></item>
						</virtual>
					</div>
				</div>
			</div>
		</div>

		<div class="mt-md-5 mt-3">
			<div class="row main-container">
				<div class="col-auto">
					<h1 class="main-header">Categories</h1>
				</div>
				<div class="col-auto align-self-center" if={"id" in category}>
					<button class="btn inline only-icon w-100 px-2" onclick={back}>
						<i class="fas fa-angle-left"></i> Back
					</button>
				</div>
				<div class="col-auto align-self-center" if={"id" in category}>
					<button class="btn inline only-icon" data-toggle="modal" data-target="#confirm-delete">
						<i class="fas fa-times"></i>
					</button>
				</div>
				<div class="w-100"></div>
				<div class="col">
					<div class="row mt-4">
						<virtual each={category in currentCategories}>
							<category name={category.name} id={category.id}></category>
						</virtual>
					</div>
				</div>
			</div>
		</div>

		<div class="mt-md-5 mt-3" if={"id" in this.category}>
			<div class="row main-container">
				<div class="col-auto">
					<h1 class="main-header">Items</h1>
				</div>
				<div class="w-100"></div>
				<div class="col">
					<div class="row mt-4">
						<virtual each={item in currentItems}>
							<item item={item}></item>
						</virtual>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<script>
		this.on('route', id => {
			id ? this.id = id : this.id = 0

			this.category = getCategory({id: this.id});
			this.currentCategories = getSubcategories({id: this.id});

			if (this.id != 0) {
				this.currentItems = getItems({category_id: this.id});
			}

			this.update()

			if (this.id == 0) {

				methods.getExpiryItems().then((data) => {
					this.expiryItems = rebuildCategories(data);
					this.update()
				})

				methods.getLowItems().then((data) => {
					this.lowItems = rebuildCategories(data.sort((a, b) => parseInt(a.ordered) - parseInt(b.ordered)));
					this.update()
				})

				methods.getAlmostLowItems().then((data) => {
					this.almostLowItems = rebuildCategories(data);
					this.update()
				})
			}
		});

		removeCategory() {
			methods.removeCategory({id: this.category.id}).always(() => {
				window.open("/stock/" + this.category.parent_category_id,"_self")
			})
		}

		back() {
			route('/stock/' + this.category.parent_category_id);
		}

		this.category = {};
		this.currentItems = [];
		this.currentCategories = [];
		this.expiryItems = [];
		this.lowItems = [];
		this.almostLowItems = [];

		
		
	</script>
</app-stock>
<manage-item>
	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                Remove item
	            </div>
	            <div class="modal-body">
	                Are you sure?
	            </div>
	            <div class="modal-footer text-center">
	                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
	                <button type="button" class="btn btn-light" data-dismiss="modal" onclick={removeItem}>Remove</button>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="modal fade" id="batch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                Expiry alert
	            </div>
	            <div class="modal-body">
	               <div class="row mt-md-5 mt-3 align-items-center">
						<div class="col">
							<p>Batch number</p>
							<input type="number" ref="batchnum" >
						</div>
						<div class="col">
							<p>Expiry date</p>
							<input type="date" ref="expiry" >
						</div>
					</div>
	            </div>
	            <div class="modal-footer text-center">
	                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
	                <button type="button" class="btn btn-light" data-dismiss="modal" onclick={confirmExpiry}>Comfirm</button>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="container">
		<div class="row main-container mt-md-5 mt-3 align-items-center">
			<div class="col-auto">
				<h1 class="main-header">Select item</h1>
			</div>
			<div class="col-auto">
				<button class="btn inline only-icon" onclick={eraseData}>
					<i class="fas fa-eraser"></i>
				</button>
			</div>
			<div class="w-100"></div>
			<div class="col">
				<div class="row">
					<div class="col mt-4">
						<p>Select stock category</p>
						<select id="category" ref="category" onchange={loadItems}>
							<option selected disabled value>Not selected</option>
							<option each={category in categories} value={category.id}>{category.name}</option>
					    </select>
					</div>
					<div class="w-100"></div>
					<div class="col mt-4 mb-4">
						<p>Select item</p>
						<select id="item" ref="item" onchange={loadDetailedItem}>
							<option selected disabled value>Not selected</option>
							<optgroup each={subcategory in Object.keys(subcategories)} label={subcategory}>
								<option each={item in subcategories[subcategory]} value={item.id}>{item.name}</option>
							</optgroup>
					    </select>
					</div>
				</div>
			</div>
		</div>

		<div class="row main-container mt-md-5 mt-3" if={Object.keys(item).length}>
			<div class="col-auto">
				<h1 class="main-header">Change stock level</h1>
			</div>
			<div class="col-auto align-self-center">
				<button class="btn inline small" data-toggle="modal" data-target="#batch" id="add-manual">Add expiry time</button>
			</div>
			<div class="w-100"></div>
			<div class="col">
				<div class="row justify-content-center align-items-center text-center mt-4" if={Object.keys(item).length}>
					<div class="col-auto" if={!parseInt(item.need_expiry_alert) || parseInt(this.item.expiry_type) != 1}>
						<button class="btn only-icon inline large" onclick={ () => decrement(1) }>
							<i class="fas fa-minus fa-lg"></i>
						</button>
					</div>

					<div class="col-auto">
						<p class="h2 m-0">Stock level</p>
						<h1 class="display-2 m-0">{currentStock}</h1>
						<p class="h3 m-0 {difference < 0 && "red" } {difference > 0 && "green" }">{difference}</p>
					</div>

					<div class="col-auto" if={!parseInt(item.need_expiry_alert) || parseInt(this.item.expiry_type) != 1}>
						<button class="btn only-icon inline large" onclick={ () => increment(1) }>
							<i class="fas fa-plus fa-lg"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row main-container my-md-5 my-3" if={Object.keys(item).length}>
			<div class="col-auto">
				<h1 class="main-header">Edit item</h1>
			</div>
			<div class="w-100"></div>
			<div class="col">
				<div class="row mb-4">
					<div class="col mt-4">
						<p>Item name</p>
						<input type="text" ref="name" value="{item.name}">
					</div>
					<div class="col mt-4">
						<p>Item type</p>
						<select id="type" ref="type">
							<option selected value=0>Not selected</option>
							<option each={type in types} value={type.id}>{type.name}</option>
					    </select>
					</div>
					<div class="col mt-4">
						<p>Low stock alert</p>
						<input type="text" ref="quantity_trigger" value="{item.quantity_trigger}">
					</div>
					<div class="col mt-4">
						<p>Orange stock alert</p>
						<input type="text" ref="orange_quantity_trigger" value="{item.orange_quantity_trigger}">
					</div>
					<div class="col mt-4">
						<p>Category</p>
						<p class="font-weight-bold">{item.category.name}</p>
					</div>
					<div class="col mt-4">
						<p>Created at</p>
						<p class="font-weight-bold">{moment(item.created_at).format('ll')}</p>
					</div>
				</div>
				<div class="row mb-4 justify-content-center align-items-center text-center">
					<div class="col mt-5">
						<button class="btn light" type="submit" onclick={ submit }>
							<span><i class="fas fa-check"></i> Submit</span>
						</button>
					</div>
					<div class="w-100"></div>
					<div class="col mt-4 mb-2">
						<button class="btn dark" data-toggle="modal" data-target="#confirm-delete">
							<span><i class="fas fa-times-circle"></i> Remove item</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		this.item = {};
		this.items = [];
		this.categories = [];
		this.subcategories = {};
		this.currentStock = 0;
		this.currentAction = 0;
		this.difference = 0;

		this.on('route', id => {
			id ? this.id = id : this.id = 0

			if (this.id != 0) {
				methods.getItem({id: this.id}).then((data) => {
					this.item = data
					this.currentStock = this.item.quantity
					this.update()
				}).then(() => {
					this.item.type != null && $('select#type option[value="' + this.item.type + '"]').prop("selected", true);

					if (this.item.category.parent_category_id == 0) {
						methods.getSubcategories({category_id: 0}).then((data) => {
							this.categories = data;

							this.update()
							$('select#category option[value="' + this.item.category.id + '"]').prop("selected", true);
						})
					} else {
						methods.getSubcategories({category_id: 0}).then((data) => {
							this.categories = data;
							this.update()
							$('select#category option[value="' + this.item.category.parent_category_id + '"]').prop("selected", true);
						})
					}
					
					methods.getItemsInCategory({category_id: this.item.category.id}).then((data) => {
						this.formatItems(data)
						this.update()
						$('select#item option[value="' + this.item.id + '"]').prop("selected", true);
					})
				})
			} else {
				methods.getSubcategories({category_id: 0}).then((data) => {
					this.categories = data;
					this.update()
				})
			}

		})

		eraseData() {
			route("/manage/item", false);
		}

		loadItems() {
			methods.getItemsInCategory({category_id: this.refs.category.selectedOptions[0].value}).then((data) => {
				this.formatItems(data);
				$('select#item').prop('selectedIndex', 0);
				this.item = {};
				this.update()
			})
		};

		loadDetailedItem() {
			route("/manage/item/" + this.refs.item.selectedOptions[0].value, false)
			methods.getItem({id: this.refs.item.selectedOptions[0].value}).then((data) => {
				this.item = data

				this.currentStock = parseInt(this.item.quantity)
				this.update()
			})
		}

		formatItems(data) {
			this.subcategories = {};

			for (var i = 0; i < data.length; i++) {
				this.subcategories[data[i].category.name] = [];
			}

			for (var i = 0; i < data.length; i++) {
				this.subcategories[data[i].category.name].push(data[i]);
			}
		}

		confirmExpiry() {
			var expiryDate = this.refs.expiry.value == "" ? "" : moment.utc(this.refs.expiry.value).format("X");
			methods.editItem({...this.item, edit_type: 3, batch_number: this.refs.batchnum.value, expiry_at: expiryDate});
		}

		increment(count = 1) {
			this.currentStock = parseInt(this.currentStock) + count;
			this.difference = parseInt(this.difference) + count;
			methods.editItem({...this.item, edit_type: 2, quantity: this.currentStock});
		}

		decrement(count = 1) {
			this.currentStock = parseInt(this.currentStock) - count;
			this.difference = parseInt(this.difference) - count;
			methods.editItem({...this.item, edit_type: 2, quantity: this.currentStock});
		}

		submit() {
			this.difference = 0;
			$('button[type=submit], input[type=submit]').prop('disabled',true);
			methods.editItem({...this.item, edit_type: 1, type: this.refs.type.selectedOptions[0].value, name: this.refs.name.value, quantity_trigger: this.refs.quantity_trigger.value, orange_quantity_trigger: this.refs.orange_quantity_trigger.value}).always(() => {
				$('button[type=submit], input[type=submit]').prop('disabled',false);
				window.open("/stock", "_self");
			})
		}

		removeItem() {
			methods.removeItem({id: this.item.id}).always(() => {
				window.open("/stock/" + this.item.category.id,"_self")
			})
			
		}
	</script>
</manage-item>
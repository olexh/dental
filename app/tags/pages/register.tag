<register>
	<div class="container">
		<form onsubmit={register}>
			<div class="row mt-md-5 mt-3 justify-content-center align-items-center" style="min-height: 100%;">
				<div class="col text-center">
					<img src="../img/logo.png" id="logo">
					<!-- <h1>Welcome</h1> -->
				</div>
				<div class="w-100"></div>
				<div class="col text-center">
					<p class="mt-2 d-inline-block">Please <a href="/login" class="toolip-btn">sign in</a> or if you are new please <a href="/register" class="toolip-btn">sign up</a></p>
				</div>
				<div class="w-100"></div>
				<div class="col-md-6 mt-5">
					<p>Type your name</p>
					<input type="text" ref="name" placeholder="John">
				</div>
				<div class="w-100"></div>
				<div class="col-md-6 mt-5">
					<p>Type your email</p>
					<input type="email" ref="email" placeholder="user@example.com">
				</div>
				<div class="w-100"></div>
				<div class="col-md-6 mt-5">
					<p>Choose dental clinic</p>
					<select id="type" ref="clinic">
						<option selected value=0>Not selected</option>
						<option each={clinic in clinics} value={clinic.id}>{clinic.name}</option>
				    </select>
				</div>
				<div class="w-100"></div>
				<div class="col-md-6 mt-5">
					<p>Type your password</p>
					<input type="password" ref="password" placeholder="*********">
				</div>
				<div class="w-100"></div>
				<div class="col-md-6 mt-5">
					<p>Confirm your password</p>
					<input type="password" ref="password_confirmation" placeholder="*********">
				</div>
				<div class="w-100"></div>
				<div class="col-md-6 mt-5 text-center">
					<button class="btn light" type="submit">
						<span><i class="fas fa-user-plus"></i> Sign up</span>
					</button>
				</div>
				<div class="w-100"></div>
				<div class="col-md-6 mt-5 text-center">
					<a href="/login"><span><i class="fas fa-sign-in-alt"></i> Sign in</span></a>
				</div>
			</div>
		</form>
	</div>
	<script>
		this.clinics = [];

		methods.getClinics().then((data) => {
			this.clinics = data;
			this.update();
		});

		register(e) {
			e.preventDefault();

			methods.signup({email: this.refs.email.value, password: this.refs.password.value, name: this.refs.name.value, password_confirmation: this.refs.password_confirmation.value, clinic_id: this.refs.clinic.selectedOptions[0].value}).then((data) => {
				window.open("/login", "_self");
			})
		}
	</script>
</register>
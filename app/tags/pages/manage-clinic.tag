<manage-clinic>
	<div class="container">
		<div class="row main-container mt-md-5 mt-3">
			<div class="col-auto">
				<h1 class="main-header">Add dental clinic</h1>
			</div>
			<div class="w-100"></div>
			<div class="col">
				<form onsubmit={ addClinic }>
					<div class="row">
						<div class="col mt-4">
							<p>Dental clinic name *</p>
							<input type="text" ref="name">
						</div>
						<div class="w-100"></div>
						<div class="col mt-3 mb-3 text-center">
							<button class="btn light" type="submit">
								<span><i class="fas fa-check"></i> Submit</span>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="row main-container mt-md-5 mt-3">
			<div class="col-auto">
				<h1 class="main-header">Dental clinics</h1>
			</div>
			<div class="w-100"></div>
			<div class="col">
				<div class="row mt-4">
					<virtual each={clinic in clinics}>
						<clinic clinic={clinic}></clinic>
					</virtual>
				</div>
			</div>
		</div>
	</div>
	<script>
		this.clinics = [];

		addClinic(e) {
			e.preventDefault()
			$('button[type=submit], input[type=submit]').prop('disabled',true);

			methods.addClinic({name: this.refs.name.value}).then((data) => {
				methods.getClinics().then((data) => {
					this.clinics = data;
					this.update();
				})
				$('button[type=submit], input[type=submit]').prop('disabled',false);
			})
		}

		methods.getClinics().then((data) => {
			this.clinics = data;
			this.update();
		})
	</script>
</manage-clinic>
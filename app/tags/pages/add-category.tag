<add-category>
	<div class="container">
		<div class="row main-container mt-md-5 mt-3">
			<div class="col-auto">
				<h1 class="main-header">Add category</h1>
			</div>
			<div class="w-100"></div>
			<div class="col">
				<form onsubmit={ addCategory }>
					<div class="row">
						<div class="col mt-4">
							<p>Category  name *</p>
							<input type="text" ref="name">
						</div>
						<div class="w-100"></div>
						<div class="col mt-4">
							<p>Stock category *</p>
							<select ref="category_select">
								<option disabled selected value>Not selected</option>
								<option each={category in categories} value={category.id}>{category.name}</option>
						    </select>
						</div>
						<div class="w-100"></div>
						<div class="col mt-3 mb-3 text-center">
							<button class="btn light" type="submit">
								<span><i class="fas fa-check"></i> Submit</span>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script>
		this.categories = [];

		methods.getCategories().then((data) => {
			this.categories = data;
			this.update()
		})

		addCategory(e) {
			e.preventDefault()
			$('button[type=submit], input[type=submit]').prop('disabled',true);
			var category_select = 0;

			for (ref in this.refs) {
				if (ref.search("param") != -1) {
					if (this.refs[ref].selectedOptions[0].value) {
						options.push(this.refs[ref].selectedOptions[0].value)
					}
				}
			}

			if (this.refs.category_select.selectedOptions[0].value != "") {
				category_select = this.refs.category_select.selectedOptions[0].value;
			}


			methods.addCategory({name: this.refs.name.value, parent_category_id: category_select}).then((data) => {
				if ("id" in data) {
					window.open("/stock/" + category_select,"_self")
				} else {
					$("#error").removeClass("d-none");
				}

				$('button[type=submit], input[type=submit]').prop('disabled',false);
			})
		}
	</script>
</add-category>
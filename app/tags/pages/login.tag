<login>
	<div class="container">
		<form onsubmit={login}>
			<div class="row mt-md-5 mt-3 justify-content-center align-items-center" style="min-height: 100%;">
				<div class="col text-center">
					<img src="../img/logo.png" id="logo">
					<!-- <h1>Welcome</h1> -->
				</div>
				<div class="w-100"></div>
				<div class="col text-center">
					<p class="mt-2 d-inline-block">Please <a href="/login" class="toolip-btn">sign in</a> or if you are new please <a href="/register" class="toolip-btn">sign up</a></p>
				</div>
				<div class="w-100"></div>
				<div class="col-md-6 mt-5">
					<p>Type your email</p>
					<input type="email" ref="email" placeholder="user@example.com">
				</div>
				<div class="w-100"></div>
				<div class="col-md-6 mt-5">
					<p>Type your password</p>
					<input type="password" ref="password" placeholder="*********">
				</div>
				<div class="w-100"></div>
				<div class="col-md-6 mt-5 text-center">
					<button class="btn light" type="submit">
						<span><i class="fas fa-sign-in-alt"></i> Sign in</span>
					</button>
				</div>
				<div class="w-100"></div>
				<div class="col-md-6 mt-5 text-center">
					<a href="/register"><span><i class="fas fa-user-plus"></i> Sign up</span></a>
				</div>
			</div>
		</form>
	</div>
	<script>
		login(e) {
			e.preventDefault();

			methods.login({email: this.refs.email.value, password: this.refs.password.value, remember: true}).then((data) => {
				if ("access_token" in data) {
					window.localStorage.setItem("access_token", data.access_token);
					window.open("/stock", "_self");
				}
			})
		}
	</script>
</login>
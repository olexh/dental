<summary>
	<div class="container">
		<div class="mt-md-5 mt-3">
			<div class="row">
				<div class="col-md-4">
					<div class="row main-container">
						<div class="col-auto">
							<h1 class="main-header">Alert summary</h1>
						</div>
						<div class="w-100"></div>
						<div class="col">
							<canvas id="low-chart" height="200"></canvas>
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="ml-md-3 mt-3 mt-md-0">
						<div class="row main-container">
							<div class="col-auto">
								<h1 class="main-header">Stock summary</h1>
							</div>
							<div class="w-100"></div>
							<div class="col">
								<div class="row mt-4">
									<div class="col-md-4">
										<p class="m-0">Categories in stock</p>
										<p class="font-weight-bold mt-1">{getCategoriesLength()}</p>
										<p class="m-0">Subcategories in stock</p>
										<p class="font-weight-bold mt-1">{getSubcategoriesLength()}</p>
										<p class="m-0">Items in stock</p>
										<p class="font-weight-bold mt-1">{items.length}</p>
									</div>
									<div class="col-md-8">
										<p class="mb-3">Amount of created items by month</p>
										<canvas id="created"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		methods.getExpiryItems().then((data) => {
			this.expiryItems = data;

			methods.getLowItems().then((data) => {
				this.lowItems = data;

				methods.getAlmostLowItems().then((data) => {
					this.almostLowItems = data;
					this.update()
					this.createLowChart()
					this.createCreatedChart()
				})
			})
		})

		getCategoriesLength() {
			return categories.filter((obj) => {
				return obj.parent_category_id == 0;
			}).length;
		}

		getSubcategoriesLength() {
			return categories.filter((obj) => {
				return obj.parent_category_id != 0;
			}).length;
		}

		getCreatedData() {
			var data = {};
			var months = moment.monthsShort();

			for (var i = 0; i < months.length; i++) {
				data[months[i]] = 0;
			}

			for (var i = 0; i < items.length; i++) {
				if (moment(items[i].created_at).format("YYYY") == moment().format("YYYY")) {
					data[moment(items[i].created_at).format("MMM")] += 1;
				}
			}

			return Object.values(data);
		}

		createLowChart() {
			var ctx = document.getElementById('low-chart').getContext('2d');
		    var data = {
			    datasets: [{
			        data: [this.expiryItems.length, this.almostLowItems.length, this.lowItems.length],
			        backgroundColor: ["grey", "orange", "red"]
			    }],

			    labels: [
			        "Expiry items", "Orange stock items", "Low stock items"
			    ]
			};

			var myPieChart = new Chart(ctx, {
				type: 'doughnut',
			    data: data,
			    options: {
				    tooltips: {
				    	borderColor: "black",
    					borderWidth: 1
				    }
			    }
			});
		}

		createCreatedChart() {
			var ctx = document.getElementById('created').getContext('2d');
			var data = {
			    datasets: [{
			        data: this.getCreatedData(),
			    }],
			    labels: moment.monthsShort()
			};

			var myBarChart = new Chart(ctx, {
			    type: 'bar',
			    data: data,
				options: {
					legend: {
				        display: false
				    }
				}
			});
		}
	</script>
</summary>
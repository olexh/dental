<manage-category>
	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                Remove item
	            </div>
	            <div class="modal-body">
	                Are you sure?
	            </div>
	            <div class="modal-footer text-center">
	                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
	                <button type="button" class="btn btn-light" data-dismiss="modal" onclick={removeItem}>Remove</button>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="container">
		<div class="row main-container mt-md-5 mt-3 align-items-center">
			<div class="col-auto">
				<h1 class="main-header">Manage category</h1>
			</div>
			<div class="col-auto">
				<button class="btn inline only-icon" onclick={eraseData}>
					<i class="fas fa-eraser"></i>
				</button>
			</div>
			<div class="w-100"></div>
			<div class="col">
				<div class="row">
					<div class="col mt-4">
						<p>Select stock category</p>
						<select id="category" ref="category" onchange={loadSubcategories}>
							<option selected disabled value>Not selected</option>
							<option each={category in categories} value={category.id}>{category.name}</option>
					    </select>
					</div>
					<div class="w-100"></div>
					<div class="col mt-4 mb-4">
						<p>Select stock subcategory</p>
						<select id="subcategory" ref="subcategory" onchange={() => loadCategory(null)}>
							<option disabled selected value=0>Not selected</option>
							<option each={subcategory in subcategories} value={subcategory.id}>{subcategory.name}</option>
					    </select>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row main-container mt-md-5 mt-3 align-items-center" if={Object.keys(category).length}>
			<div class="col-auto">
				<h1 class="main-header">Edit category</h1>
			</div>
			<div class="w-100"></div>
			<div class="col">
				<form onsubmit={ editCategory }>
					<div class="row">
						<div class="col mt-4">
							<p>Category name</p>
							<input type="text" ref="name" value="{category.name}">
						</div>
						<div class="w-100"></div>
						<div class="col mt-4 mb-4 text-center">
							<button class="btn light" type="submit">
								<span><i class="fas fa-check"></i> Submit</span>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		
	</div>
	<script>
		this.category = {};
		this.categories = [];
		this.subcategories = [];

		this.on('route', id => {
			id ? this.id = id : this.id = 0

			if (this.id != 0) {
				methods.getCategory({id}).then((data) => {
					this.category = data;
					this.update()
				}).then(() => {
					if (this.category.parent_category_id == 0) {
						methods.getSubcategories({category_id: 0}).then((data) => {
							this.categories = data;

							this.update()
							$('select#category option[value="' + this.category.id + '"]').prop("selected", true);
						})

						methods.getSubcategories({category_id: this.category.id}).then((data) => {
							this.subcategories = data;

							this.update()
						})
					} else {
						methods.getSubcategories({category_id: 0}).then((data) => {
							this.categories = data;

							this.update()
							$('select#category option[value="' + this.category.parent_category_id + '"]').prop("selected", true);
						})

						methods.getSubcategories({category_id: this.category.parent_category_id}).then((data) => {
							this.subcategories = data;

							this.update()
							$('select#subcategory option[value="' + this.category.id + '"]').prop("selected", true);
						})
					}
					
				})
			} else {
				methods.getSubcategories({category_id: 0}).then((data) => {
					this.categories = data;
					this.update()
				})
			}

		})

		eraseData() {
			route("/manage/category", false);
		}

		loadSubcategories() {
			$('select#subcategory').prop('selectedIndex', 0);
			this.loadCategory(this.refs.category.selectedOptions[0].value);
			/*methods.getSubcategories({category_id: this.refs.category.selectedOptions[0].value}).then((data) => {
				this.subcategories = data;
				this.update()
			})*/

			
		};

		loadCategory(caterogyId) {
			if (caterogyId == null) {
				route("/manage/category/" + this.refs.subcategory.selectedOptions[0].value, false)
			} else {
				route("/manage/category/" + caterogyId, false)
			}			
		}

		editCategory(e) {
			e.preventDefault()
			console.log('launched')
			$('button[type=submit], input[type=submit]').prop('disabled',true);
			methods.editCategory({...this.category, name: this.refs.name.value}).always(() => {
				window.open("/stock/" + this.category.parent_category_id,"_self");
				$('button[type=submit], input[type=submit]').prop('disabled',false);
			})
		}

		removeCategory() {
			methods.removeCategory({id: this.category.id}).always(() => {
				window.open("/stock/" + this.category.parent_category_id,"_self")
			})
		}
	</script>
</manage-category>
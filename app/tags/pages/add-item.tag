<add-item>
	<div class="modal fade" id="add-manual-modal" tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                Add manual time
	            </div>
	            <div class="modal-body">
	               <div class="row align-items-center">
						<div class="col">
							<p>Expiry date</p>
							<input type="date" ref="expiry" >
						</div>
					</div>
	            </div>
	            <div class="modal-footer text-center">
	                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
	                <button type="button" class="btn btn-light" data-dismiss="modal" onclick={confirmExpiry}>Comfirm</button>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="container">
		<div class="row main-container mt-md-5 mt-3">
			<div class="col-auto">
				<h1 class="main-header">Add item</h1>
			</div>
			<div class="w-100"></div>
			<div class="col">
				<form onsubmit={ addItem }>
					<div class="row">
						<div class="col-md-3 mt-4">
							<p>Stock item name *</p>
							<input type="text" ref="name">
						</div>
						<div class="col-md-3 mt-4">
							<p>Quantity *</p>
							<input type="number" ref="quantity">
						</div>
						<div class="col-md-3 mt-4">
							<p>Low stock alert</p>
							<input type="number" ref="quantity_trigger">
						</div>
						<div class="col-md-3 mt-4">
							<p>Orange zone alert</p>
							<input type="number" ref="orange_quantity_trigger">
						</div>
						<div class="w-100"></div>
						<div class="col mt-4">
							<p>Item type</p>
							<select ref="type">
								<option disabled selected value=0>Not selected</option>
								<option each={type in types} value={type.id}>{type.name}</option>
						    </select>
						</div>
						<div class="w-100"></div>
						<div class="col mt-4">
							<p>Stock category *</p>
							<select ref="category" onchange={loadSubcategories}>
								<option disabled selected value=0>Not selected</option>
								<option each={category in categories} value={category.id}>{category.name}</option>
						    </select>
						</div>
						<div class="w-100"></div>
						<div class="col mt-4">
							<p>Stock subcategory *</p>
							<select ref="subcategory">
								<option disabled selected value=0>Not selected</option>
								<option each={subcategory in subcategories} value={subcategory.id}>{subcategory.name}</option>
						    </select>
						</div>
						<div class="w-100"></div>
						<div class="col mt-3 mb-3 text-center">
							<p class="d-none" id="error">Something went wrong. Please, try again!</p>
							<button class="btn light" type="submit">
								<span><i class="fas fa-check"></i> Submit</span>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		
	</div>
	<script>
		this.categories = [];
		this.subcategories = [];
		this.params = [];
		this.types = [{id: 1, name: "Box"}, {id: 2, name: "Pack"}, {id: 3, name: "Tube"}, {id: 4, name: "Bottle"}, {id: 5, name: "Singular"}];

		this.needExpiry = 0;
		this.expiryType = 1;
		this.expiryDate = "";

		methods.getSubcategories({category_id: 0}).then((data) => {
			this.categories = data;
			this.update()
		})

		checkedExpiry() {
			if (this.refs.need_expiry_alert.checked) {
				$('#add-manual').removeClass("d-none");
				this.needExpiry = 1;
			} else {
				$('#add-manual').addClass("d-none");
				this.needExpiry = 0;
			}

		}
		
		loadSubcategories() {
			$('select#subcategory').prop('selectedIndex', 0);
			methods.getSubcategories({category_id: this.refs.category.selectedOptions[0].value}).then((data) => {
				this.subcategories = data;
				this.update()
			})
		}

		confirmExpiry() {
			console.log(this.refs.expiry.value)
			if (this.refs.expiry.value != "") {
				this.expiryType = 2;
				this.expiryDate = moment.utc(this.refs.expiry.value).format("X");
			}
		}

		addItem(e) {
			e.preventDefault()
			$('button[type=submit], input[type=submit]').prop('disabled',true);
			var options = [];

			for (ref in this.refs) {
				if (ref.search("param") != -1) {
					if (this.refs[ref].selectedOptions[0].value) {
						options.push(this.refs[ref].selectedOptions[0].value)
					}
				}
			}

			let selected = this.refs.subcategory.selectedOptions[0].value != 0 ? this.refs.subcategory.selectedOptions[0].value : this.refs.category.selectedOptions[0].value;

			if (selected == 0) {
				$('button[type=submit], input[type=submit]').prop('disabled',false);
				return;
			}

			methods.addItem({name: this.refs.name.value, quantity_trigger: this.refs.quantity_trigger.value, orange_quantity_trigger: this.refs.orange_quantity_trigger.value, quantity: this.refs.quantity.value, category_id: selected, options, type: this.refs.type.value, need_expiry_alert: this.needExpiry, expiry_type: this.expiryType, expiry_at: this.expiryDate}).always((data) => {
				// window.open("/stock/", "_self");

				$('button[type=submit], input[type=submit]').prop('disabled',false);
			})
		}
	</script>
</add-item>